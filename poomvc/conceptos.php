<!--
- conectarse a mysql -> mysql -u nombre_usuario -p
- acceder a la base de datos -> USE nombre_base_datos;
- crear una tabla ->
CREATE TABLE employees (
    id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    dni VARCHAR(20) UNIQUE, -> esta restricción es para que solo haya un registro con el valor del dni
    name VARCHAR(150),
    email VARCHAR(100),
    birth_date DATE,
    created_at TIMESTAMP NULL,
    updated_at TIMESTAMP NULL  
);

- visualizar un tabla -> SHOW TABLES;
- visualizar la estructura de una tabla -> DESCRIBE autos;
- eliminar una tabla -> DROP TABLE autos;
- consultar los datos de una tabla -> SELECT auto_id, make, year, mileage FROM autos;
- actualizar una tabla de datos -> UPDATE autos SET year = '2021' WHERE id = 2; (si no incluimos el WHERE 
                                            MODIFICAMOS TODOS LOS CAMPOS year DE TODAS LAS FILAS DE LA TABLA)
- eliminar datos de una tabla -> DELETE FROM autos WHERE id = 2;

- insertar datos en una tabla -> INSERT INTO autos ( make, year,mileage) 
                                VALUES ('FABRICADO EN ESPAÑA', '2020', '1000');
-                                 
- strrev - funcion que invierte un string


En PDOStatement::bindValue() se asigna el valor que tenga en ese momento la variable y aunque ésta cambie 
a lo largo de varias ejecuciones de execute() la sustitución permanece invariable.

Ambas protegen por igual de inyección SQL y tienen exactamente el mismo comportamiento interno una vez 
resueltas las sustituciones.



-->